# Test Suite

This Test Suite aims to test the interoperability between integrators and implementors of the VUI presentation exchange
API.

The Presentation Exchange API and protocols are still being defined and tested, following different standarization
efforts.

The role of the implementor is that of the Verifier. The role of the integrator would represent the Holder.

## Getting Started

> TBD - Instructions, how to setup and launch the test suite.

## Test Cases for Presentation Exchange

The interoperability testing will be executed across different phases; matching the different versions of the API and
including progressively new protocols and features by little increments.

### Phase 1: [API v1](../VUI-PresentationExchange-openapi.yaml)

Phase 1 aims to test the API v1. This API only allows for the
exchange [DIF Presentation Exchange](https://identity.foundation/presentation-exchange/). For that goal, Sphereon offers
a [Typescript implementation of the library](https://github.com/Sphereon-Opensource/pe-js) to generate the expected
documents following the defined data model.

#### Test Status

| Case | Description | Sphereon Status | Gataca Status |
|---|---|---|---|
| 1 | Submit a verifiable presentation for a definition containing only one input descriptor with no constraints. | [ ]  | [ ] |
| 2 | Submit a verifiable presentation for a definition containing only one input descriptor with constraints. | [ ]  | [ ] |
| 3 | Submit a verifiable presentation for a definition containing more than one input descriptor with no constraints. | [ ]  | [ ] |
| 4 | Submit a verifiable presentation for a definition containing more than one input descriptor with constraints. | [ ]  | [ ] |
| 5 | Submit a verifiable presentation for a definition containing submission requirements | [ ]  | [ ] |
| 6 | Create a presentation definition | [ ]  | [ ] |
| 7 | Retrieve a presentation definition | [ ]  | [ ] |

#### Testing assumptions:

* Credential schemas to be used: TBD
* Presentation definition and constraints: TBD
* Presentation format: ldp_vp || ldp_vc
* Keys for verification: ED25519
* Signature suite: Ed25519VerificationKey2018

````json 
{
  "format": {
      "ldp_vc": {
        "proof_type": [
          "Ed25519Signature2018",
        ]
      },
      "ldp_vp": {
        "proof_type": ["Ed25519Signature2018"]
      },
  }
}
````

#### Case 1

Submit a verifiable presentation for a definition containing a simple input_descriptor with no constraints . The checks
to be performed on this case are:

- Cryptographic validation of the Verifiable presentation: ``vpproof``
- Cryptographic validation of the verifiable credential submitted: ``vcproof``
- Schema validation of the verifiable credential: ``schema``
- Matching of ids of the presentation and satisfaction of the input descriptor selected: ``definition``

##### Case succeeds

- should return ``200`` on assumption X

```json
{
"checks": [
  "vpproof",
  "definition",
  "vcproof",
  "schema",
  ],
"warnings": [ ],
"errors": [ ]
}
```

##### Case fails

- should return `400` when the submission is invalid or malformed
- should return `401` when any of the checks is failing

```json
{
"checks": [
  "vpproof",
  "definition",
  "vcproof",
  "schema",
  ],
"warnings": [ ],
"errors": [
  "$.vc[0] proof couldn't be validated",
]
}
```

- should return `413` when the submission data is too large
- should return `429` when a defined rate limit is exceeded
- should return `500` when the server fails, crashes or panics during validation

#### Case 2

Submit a verifiable presentation for a definition containing a simple input_descriptor with constraints. The checks to
be performed on this case are the same as in case 1 with the addition of:

- Required constraints must be satisfied: ``constraints``

##### Case succeeds

- should return ``200`` on assumption X

```json
{
"checks": [
  "vpproof",
  "definition",
  "vcproof",
  "schema",
  "constraints"
  ],
"warnings": [ ],
"errors": [ ]
}
```

##### Case fails

- should return `400` when the submission is invalid or malformed
- should return `401` when any of the checks is failing

```json
{
"checks": [
  "vpproof",
  "definition",
  "vcproof",
  "schema",
  "constraints"
  ],
"warnings": [ ],
"errors": [
  "No credential satisfying constraint X was found in the presentation",
]
}
```

- should return `413` when the submission data is too large
- should return `429` when a defined rate limit is exceeded
- should return `500` when the server fails, crashes or panics during validation

#### Case 3

Submit a verifiable presentation for a definition containing a multiple input_descriptor with no constraints. The checks
to be performed on this case are:

- Cryptographic validation of the Verifiable presentation: ``vpproof``
- Cryptographic validation of the verifiable credential submitted: ``vcproof``
- Schema validation of the verifiable credential: ``schema``
- Matching of ids of the presentation and satisfaction of the input descriptor selected: ``definition``

##### Case succeeds

- should return ``200`` on assumption X

```json
{
"checks": [
  "vpproof",
  "definition",
  "vcproof",
  "schema",
  "constraints",
  ],
"warnings": [ ],
"errors": [ ]
}
```

##### Case fails

- should return `400` when the submission is invalid or malformed
- should return `401` when any of the checks is failing

```json
{
"checks": [
  "vpproof",
  "definition",
  "vcproof",
  "schema",
  "constraints"
  ],
"warnings": [ ],
"errors": [
  "$.vc[0] proof couldn't be validated",
]
}
```

- should return `413` when the submission data is too large
- should return `429` when a defined rate limit is exceeded
- should return `500` when the server fails, crashes or panics during validation

#### Case 4

Submit a verifiable presentation for a definition containing a multiple input_descriptor with constraints. The checks to
be performed on this case are the same as in case 2 and 3.

##### Case succeeds

- should return ``200`` on assumption X

```json
{
"checks": [
  "vpproof",
  "definition",
  "vcproof",
  "schema",
  "constraints"
  ],
"warnings": [ ],
"errors": [ ]
}
```

##### Case fails

- should return `400` when the submission is invalid or malformed
- should return `401` when any of the checks is failing

```json
{
"checks": [
  "vpproof",
  "definition",
  "vcproof",
  "schema",
  "constraints",
  ],
"warnings": [ ],
"errors": [
  "No credential satisfying constraint X was found in the presentation",
]
}
```

- should return `413` when the submission data is too large
- should return `429` when a defined rate limit is exceeded
- should return `500` when the server fails, crashes or panics during validation

#### Case 5

Submit a verifiable presentation for a definition containing multiple input_descriptor, contraints and submission
requirements. The checks to be performed on this case are the same as in case 2,3,4 with the addition of:

- Submission requirements must be satisfied: ``submission``

##### Case succeeds

- should return ``200`` on assumption X

```json
{
"checks": [
  "vpproof",
  "definition",
  "vcproof",
  "schema",
  "constraints",
  "constraints"
  ],
"warnings": [ ],
"errors": [ ]
}
```

##### Case fails

- should return `400` when the submission is invalid or malformed
- should return `401` when any of the checks is failing

```json
{
"checks": [
  "vpproof",
  "definition",
  "vcproof",
  "schema",
  "constraints",
  "submission"
  ],
"warnings": [ ],
"errors": [
  "No credential satisfying constraint X was found in the presentation",
]
}
```

- should return `413` when the submission data is too large
- should return `429` when a defined rate limit is exceeded
- should return `500` when the server fails, crashes or panics during validation

#### Case 6

````
TBD Upon API Acceptance
````
