import {DataProvider} from './DataProvider';
import {DataProviders} from "./DataProviders";

export class SubmitVerifiablePresentationMultipleInputDescriptorWithConstraintsDataProvider extends DataProvider {
  getRequestData(implementor: string): any {
    const response = DataProviders.getInstance().getDataProvider(implementor, 'presentation_definition_multiple_input_descriptor_with_constraints').getResponse();
    return {
      "thread": {
        "id": response?.thread?.id
      },
      "id": "58d0664f-a715-413e-90e9-19ea45e07533",
      "comment": "Create PD test",
      "presentation": {
        "@context": [
          "https://www.w3.org/2018/credentials/v1",
          "https://identity.foundation/presentation-exchange/submission/v1"
        ],
        "presentation_submission": {
          "id": "540a1142-4b00-459d-a73d-0974e982579e",
          "definition_id": "4506ce35-d27a-4901-971b-1d45e69b83fc",
          "descriptor_map": [
            {
              "id": "867bfe7a-5b91-46b2-9ba4-70028b8d9cc8",
              "format": "ldp_vp",
              "path": "$.verifiableCredential[0]"
            }
          ]
        },
        "type": [
          "VerifiablePresentation",
          "PresentationSubmission"
        ],
        "verifiableCredential": [
          {
            "@context": [
              "https://www.w3.org/2018/credentials/v1",
              "https://w3c-ccg.github.io/vc-examples/covid-19/v2/v2.jsonld"
            ],
            "id": "http://example.com/credential/123",
            "type": [
              "VerifiableCredential",
              "qSARS-CoV-2-Travel-Badge-Credential"
            ],
            "issuer": {
              "id": "did:elem:ropsten:EiBJJPdo-ONF0jxqt8mZYEj9Z7FbdC87m2xvN0_HAbcoEg",
              "location": {
                "@type": "CovidTestingFacility",
                "name": "Stanford Health Care",
                "url": "https://stanfordhealthcare.org/"
              }
            },
            "issuanceDate": "2019-12-11T03:50:55Z",
            "expirationDate": "2020-12-11T03:50:55Z",
            "name": "qSARS-CoV-2 Travel Badge Credential",
            "description": "This card is accepted, under specified conditions, as proof of medical clearance and for identification of the holder’s medical condition. See https://www.who.int/ith/mode_of_travel/travellers/en/.",
            "credentialSubject": {
              "id": "did:key:z6MkjRagNiMu91DduvCvgEsqLZDVzrJzFrwahc4tXLt9DoHd",
              "type": [
                "Person"
              ],
              "image": "https://cdn.pixabay.com/photo/2014/10/26/21/42/man-504453_1280.jpg"
            },
            "proof": {
              "type": "Ed25519Signature2018",
              "created": "2020-04-18T18:35:13Z",
              "verificationMethod": "did:elem:ropsten:EiBJJPdo-ONF0jxqt8mZYEj9Z7FbdC87m2xvN0_HAbcoEg#xqc3gS1gz1vch7R3RvNebWMjLvBOY-n_14feCYRPsUo",
              "proofPurpose": "assertionMethod",
              "jws": "eyJhbGciOiJFZERTQSIsImI2NCI6ZmFsc2UsImNyaXQiOlsiYjY0Il19..pbZdiR4OtS6dqW2Dw8-oiQMXJiAsXJHufaAQrhYBNVL7bcXlVd_mCkr4fkigDR7X6O4J64Yqn2mBhbIgupDVBw"
            }
          }
        ]
      },
      "challenge": {
        "token": response?.challenge?.token
      },
      "pdId": "4506ce35-d27a-4901-971b-1d45e69b83fc",
      "callback": {
        "url": "http://127.0.0.1:3000/presentations/58d0664f-a715-413e-90e9-19ea45e07533/statuses"
      }
    };
  }

  setResponseData(response: any) {
    this.response = response;
  }

  public getTag() {
    return 'submit_verifiable_presentation_multiple_input_descriptor_with_constraints';
  }
}
