import { Implementor } from '../test/models';

import { CreatePresentationDataProvider } from './CreatePresentationDataProvider';
import { CreatePresentationDefinitionDataProvider } from './CreatePresentationDefinitionDataProvider';
import { CreateThreadDataProvider } from './CreateThreadDataProvider';
import { DataProvider } from './DataProvider';
import { PresentationDefinition1InputDescriptorNoConstraintsDataProvider } from './PresentationDefinition1InputDescriptorNoConstraintsDataProvider';
import { PresentationDefinition1InputDescriptorWithConstraintsDataProvider } from './PresentationDefinition1InputDescriptorWithConstraintsDataProvider';
import { PresentationDefinitionMultipleInputDescriptorNoConstraintsDataProvider } from './PresentationDefinitionMultipleInputDescriptorNoConstraintsDataProvider';
import { PresentationDefinitionMultipleInputDescriptorWithConstraintsDataProvider } from './PresentationDefinitionMultipleInputDescriptorWithConstraintsDataProvider';
import { PresentationDefinitionMultipleInputDescriptorWithSubmissionRequirementsDataProvider } from './PresentationDefinitionMultipleInputDescriptorWithSubmissionRequirementsDataProvider';
import { RetrievePresentationDataProvider } from './RetrievePresentationDataProvider';
import { RetrievePresentationDefinitionDataProvider } from './RetrievePresentationDefinitionDataProvider';
import { SubmitVerifiablePresentation1InputDescriptorNoConstraintsDataProvider } from './SubmitVerifiablePresentation1InputDescriptorNoConstraintsDataProvider';
import { SubmitVerifiablePresentation1InputDescriptorWithConstraintsDataProvider } from './SubmitVerifiablePresentation1InputDescriptorWithConstraintsDataProvider';
import { SubmitVerifiablePresentationDataProvider } from './SubmitVerifiablePresentationDataProvider';
import { SubmitVerifiablePresentationMultipleInputDescriptorNoConstraintsDataProvider } from './SubmitVerifiablePresentationMultipleInputDescriptorNoConstraintsDataProvider';
import { SubmitVerifiablePresentationMultipleInputDescriptorWithConstraintsDataProvider } from './SubmitVerifiablePresentationMultipleInputDescriptorWithConstraintsDataProvider';
import { SubmitVerifiablePresentationMultipleInputDescriptorWithSubmissionRequirementsDataProvider } from './SubmitVerifiablePresentationMultipleInputDescriptorWithSubmissionRequirementsDataProvider';

export class DataProviders {
  private static instance: DataProviders;

  private exchangers: Map<string, Map<string, DataProvider>>;
  private dataProviders: Map<string, DataProvider>;

  private constructor() {
    this.exchangers = new Map<string, Map<string, DataProvider>>();

    this.exchangers.set(Implementor.sphereon.toString(), this.initDataProviders());
    this.exchangers.set(Implementor.gataca.toString(), this.initDataProviders());
  }

  public static getInstance(): DataProviders {
    if (!DataProviders.instance) {
      DataProviders.instance = new DataProviders();
    }

    return DataProviders.instance;
  }

  addDP(dp: DataProvider) {
    this.dataProviders.set(dp.getTag(), dp);
  }

  public getDataProvider(implementor: string, tag: string): DataProvider {
    return this.exchangers.get(implementor).get(tag);
  }

  private initDataProviders(): Map<string, DataProvider> {
    this.dataProviders = new Map<string, DataProvider>();

    this.addDP(new CreateThreadDataProvider());
    this.addDP(new CreatePresentationDefinitionDataProvider());
    this.addDP(new RetrievePresentationDefinitionDataProvider());
    this.addDP(new CreatePresentationDataProvider());
    this.addDP(new RetrievePresentationDataProvider());
    this.addDP(new SubmitVerifiablePresentationDataProvider());

    this.addDP(new PresentationDefinition1InputDescriptorNoConstraintsDataProvider());
    this.addDP(new SubmitVerifiablePresentation1InputDescriptorNoConstraintsDataProvider());

    this.addDP(new PresentationDefinition1InputDescriptorWithConstraintsDataProvider());
    this.addDP(new SubmitVerifiablePresentation1InputDescriptorWithConstraintsDataProvider());

    this.addDP(new PresentationDefinitionMultipleInputDescriptorNoConstraintsDataProvider());
    this.addDP(new SubmitVerifiablePresentationMultipleInputDescriptorNoConstraintsDataProvider());

    this.addDP(new PresentationDefinitionMultipleInputDescriptorWithConstraintsDataProvider());
    this.addDP(new SubmitVerifiablePresentationMultipleInputDescriptorWithConstraintsDataProvider());

    this.addDP(new PresentationDefinitionMultipleInputDescriptorWithSubmissionRequirementsDataProvider());
    this.addDP(new SubmitVerifiablePresentationMultipleInputDescriptorWithSubmissionRequirementsDataProvider());

    return this.dataProviders;
  }
}
