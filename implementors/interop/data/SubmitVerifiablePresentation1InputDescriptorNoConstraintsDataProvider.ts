import { DataProvider } from './DataProvider';
import {DataProviders} from "./DataProviders";

export class SubmitVerifiablePresentation1InputDescriptorNoConstraintsDataProvider extends DataProvider {
  getRequestData(implementor: string): any {
    const response = DataProviders.getInstance().getDataProvider(implementor, 'presentation_definition_1_input_descriptor_no_constraints').getResponse();
    return {
      "thread": {
        "id": response?.thread?.id
      },
      "id": "58d0664f-a715-413e-90e9-19ea45e07533",
      "comment": "Create PD test",
      "presentation": {
        "@context": [
          "https://www.w3.org/2018/credentials/v1",
          "https://identity.foundation/presentation-exchange/submission/v1"
        ],
        "presentation_submission": {
          "id": "540a1142-4b00-459d-a73d-0974e982579e",
          "definition_id": "4506ce35-d27a-4901-971b-1d45e69b83fc",
          "descriptor_map": [
            {
              "id": "867bfe7a-5b91-46b2-9ba4-70028b8d9cc8",
              "format": "ldp_vp",
              "path": "$.verifiableCredential[0]"
            }
          ]
        },
        "type": [
          "VerifiablePresentation",
          "PresentationSubmission"
        ],
        "verifiableCredential": [
          {
            "@context": [
              "https://www.w3.org/2018/credentials/v1"
            ],
            "credentialSchema": [
              {
                "id": "https://www.w3.org/TR/vc-data-model/#types"
              }
            ],
            "credentialSubject": {
              "age": 19,
              "etc": "etc"
            },
            "id": "2dc74354-e965-4883-be5e-bfec48bf60c7",
            "issuer": "",
            "type": "VerifiableCredential",
            "proof": {
              "type": "BbsBlsSignatureProof2020",
              "created": "2020-04-25",
              "verificationMethod": "did:example:489398593#test",
              "proofPurpose": "assertionMethod",
              "proofValue": "kTTbA3pmDa6Qia/JkOnIXDLmoBz3vsi7L5t3DWySI/VLmBqleJ/Tbus5RoyiDERDBEh5rnACXlnOqJ/U8yFQFtcp/mBCc2FtKNPHae9jKIv1dm9K9QK1F3GI1AwyGoUfjLWrkGDObO1ouNAhpEd0+et+qiOf2j8p3MTTtRRx4Hgjcl0jXCq7C7R5/nLpgimHAAAAdAx4ouhMk7v9dXijCIMaG0deicn6fLoq3GcNHuH5X1j22LU/hDu7vvPnk/6JLkZ1xQAAAAIPd1tu598L/K3NSy0zOy6obaojEnaqc1R5Ih/6ZZgfEln2a6tuUp4wePExI1DGHqwj3j2lKg31a/6bSs7SMecHBQdgIYHnBmCYGNQnu/LZ9TFV56tBXY6YOWZgFzgLDrApnrFpixEACM9rwrJ5ORtxAAAAAgE4gUIIC9aHyJNa5TBklMOh6lvQkMVLXa/vEl+3NCLXblxjgpM7UEMqBkE9/QcoD3Tgmy+z0hN+4eky1RnJsEg=",
              "nonce": "6i3dTz5yFfWJ8zgsamuyZa4yAHPm75tUOOXddR6krCvCYk77sbCOuEVcdBCDd/l6tIY="
            }
          }
        ]
      },
      "challenge": {
        "token": response?.challenge?.token
      },
      "pdId": "4506ce35-d27a-4901-971b-1d45e69b83fc",
      "callback": {
        "url": "http://127.0.0.1:3000/presentations/58d0664f-a715-413e-90e9-19ea45e07533/statuses"
      }
    };
  }

  setResponseData(response: any) {
    this.response = response;
  }

  public getTag() {
    return 'submit_verifiable_presentation_1_input_descriptor_no_constraints';
  }
}
