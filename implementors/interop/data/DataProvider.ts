export abstract class DataProvider {
  response: any;

  abstract getRequestData(implementor: string): any;

  abstract getTag();

  getQueryParamData(implementor: string): string {
    const params = this.getQueryParams(implementor);

    let qs = '';
    if (params != null) {
      params.forEach((value: string, key: string) => {
        if (qs.length > 0) {
          qs += '&';
        }
        const queryParam = key + '=' + value;
        qs += queryParam;
      });

      qs = '?' + qs;
    }

    return qs;
  }

  getQueryParams(implementor: string): Map<string, string> {
    return implementor != null ? null : null; // No Op, always returns null;
  }

  setResponseData(response: any) {
    this.response = response;
  }

  getResponse(): any {
    return this.response;
  }
}
