import { DataProvider } from './DataProvider';
import { DataProviders } from './DataProviders';

export class PresentationDefinitionMultipleInputDescriptorWithSubmissionRequirementsDataProvider extends DataProvider {
  getRequestData(implementor: string): any {
    const response = DataProviders.getInstance().getDataProvider(implementor, 'submit_verifiable_presentation_multiple_input_descriptor_with_constraints').getResponse();
    return {
      thread: {
        id: response?.thread.id,
      },
      id: '0471841e-feee-40cc-b071-24c4df39f065',
      comment: 'Create PD test',
      presentation_definition: {
        submission_requirements: [
          {
            from: 'A',
            purpose: "We need your driver's license name",
            rule: 'all',
            name: "Name on driver's license",
          },
        ],
        id: '32f54163-7166-48f1-93d8-ff217bdb0654',
        input_descriptors: [
          {
            id: 'citizenship_input_1',
            name: "EU Driver's License",
            group: ['A'],
            constraints: {
              fields: [
                {
                  path: ['$.vc.@context', '$.@context'],
                  filter: {
                    type: 'string',
                    _const: 'https://eu.com/claims/DriversLicense',
                  },
                },
              ],
            },
          },
          {
            id: 'citizenship_input_2',
            name: "EU Driver's License",
            group: ['A'],
            constraints: {
              fields: [
                {
                  path: ['$.iss', '$.vc.issuer', '$.issuer'],
                  filter: {
                    type: 'string',
                    _const: 'did:example:123',
                  },
                },
              ],
            },
          },
        ],
        format: {
          ldp_vp: {
            proof_type: [
              'Ed25519Signature2018'
            ]
          }
        }
      },
      callback: {
        url: 'http://127.0.0.1:3000/pe/v1/definitions/4506ce35-d27a-4901-971b-1d45e69b83fc/status'
      },
      challenge: {
        token: response?.challenge?.token
      }
    };
  }

  getQueryParams(implementor: string): Map<string, string> {
    const response = DataProviders.getInstance().getDataProvider(implementor, 'submit_verifiable_presentation_multiple_input_descriptor_with_constraints').getResponse();
    let queryParams = null;
    if (response) {
      const token: string = response.challenge?.token;
      const threadId: string = response.thread?.id;

      queryParams = new Map<string, string>();
      if (token) {
        queryParams.set('token', token);
      }
      if (threadId) {
        queryParams.set('thread_id', threadId);
      }
    }
    return queryParams;
  }

  setResponseData(response: any) {
    this.response = response;
  }

  public getTag() {
    return 'presentation_definition_multiple_input_descriptor_with_submission_requirements';
  }
}
