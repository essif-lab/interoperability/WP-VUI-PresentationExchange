import { DataProvider } from './DataProvider';
import { DataProviders } from './DataProviders';

export class CreatePresentationDefinitionDataProvider extends DataProvider {
  getRequestData(implementor: string): any {
    const response = DataProviders.getInstance().getDataProvider(implementor, 'create_thread').getResponse();
    return {
      "thread": {
        "id": response?.id
      },
      "id": "4506ce35-d27a-4901-971b-1d45e69b83fc",
      "comment": "Create PD test",
      "presentation_definition": {
        "id": "4506ce35-d27a-4901-971b-1d45e69b83fc",
        "input_descriptors": [
          {
            "schema": [
              {
                "uri": "https://www.w3.org/2018/credentials/v1"
              }
            ],
            "name": "EU Driver's License",
            "id": "citizenship_input_1"
          }
        ],
        "format": {
          "ldp_vp": {
            "proof_type": [
              "Ed25519Signature2018"
            ]
          }
        }
      },
      "callback": {
        "url": "http://127.0.0.1:3000/pe/v1/definitions/4506ce35-d27a-4901-971b-1d45e69b83fc/status"
      },
      "challenge": {
        "token": response?.challenge?.token
      }
    };
  }

  setResponseData(response: any) {
    this.response = response;
  }

  public getTag() {
    return 'create_presentation_definition';
  }
}
