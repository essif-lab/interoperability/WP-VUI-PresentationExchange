import {DataProvider} from './DataProvider';
import {DataProviders} from "./DataProviders";

export class RetrievePresentationDefinitionDataProvider extends DataProvider {
  getRequestData(): any {
    return null;
  }

  getQueryParams(implementor: string): Map<string, string> {
    const response = DataProviders.getInstance().getDataProvider(implementor, 'create_presentation_definition').getResponse();
    let queryParams = null;
    if (response) {
      const token: string = response.challenge?.token;
      const threadId: string = response.thread?.id;

      queryParams = new Map<string, string>();
      if (token) {
        queryParams.set('token', token);
      }
      if (threadId) {
        queryParams.set('thread_id', threadId);
      }
    }
    return queryParams;
  }

  setResponseData(response: any) {
    this.response = response;
  }

  public getTag() {
    return 'retrieve_presentation_definition';
  }
}
