import { DataProvider } from './DataProvider';

export class CreateThreadDataProvider extends DataProvider {
  getRequestData(): any {
    return null;
  }

  setResponseData(response: any) {
    this.response = response;
  }

  public getTag() {
    return 'create_thread';
  }
}
