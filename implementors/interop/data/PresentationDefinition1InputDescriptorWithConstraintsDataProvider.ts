import { DataProvider } from './DataProvider';
import { DataProviders } from './DataProviders';

export class PresentationDefinition1InputDescriptorWithConstraintsDataProvider extends DataProvider {
  getRequestData(implementor: string): any {
    const response = DataProviders.getInstance().getDataProvider(implementor, 'submit_verifiable_presentation_1_input_descriptor_no_constraints').getResponse();
    return {
      thread: {
        id: response?.thread?.id
      },
      id: '4506ce35-d27a-4901-971b-1d45e69b83fc',
      comment: 'Create PD test',
      presentation_definition: {
        id: '4506ce35-d27a-4901-971b-1d45e69b83fc',
        input_descriptors: [
          {
            id: 'wa_driver_license',
            name: 'Submission of wa drivers license',
            schema: [
              {
                uri: 'https://eu.com/claims/DriversLicense',
              }
            ],
            constraints: {
              fields: [
                {
                  path: ['$.issuer', '$.vc.issuer', '$.iss'],
                  filter: {
                    type: 'string',
                    pattern: 'did:example:123|did:example:456',
                  },
                },
              ],
            },
          }
        ],
        format: {
          ldp_vp: {
            proof_type: [
              'Ed25519Signature2018'
            ]
          }
        }
      },
      callback: {
        url: 'http://127.0.0.1:3000/pe/v1/definitions/4506ce35-d27a-4901-971b-1d45e69b83fc/status'
      },
      challenge: {
        token: response?.challenge?.token
      }
    };
  }

  getQueryParams(implementor: string): Map<string, string> {
    const response = DataProviders.getInstance().getDataProvider(implementor, 'create_presentation_definition').getResponse();
    let queryParams = null;
    if (response) {
      const token: string = response.challenge?.token;
      const threadId: string = response.thread?.id;

      queryParams = new Map<string, string>();
      if (token) {
        queryParams.set('token', token);
      }
      if (threadId) {
        queryParams.set('thread_id', threadId);
      }
    }
    return queryParams;
  }

  setResponseData(response: any) {
    this.response = response;
  }

  public getTag() {
    return 'presentation_definition_1_input_descriptor_with_constraints';
  }
}
