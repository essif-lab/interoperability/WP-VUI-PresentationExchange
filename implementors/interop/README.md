DIF Presentation Exchange Implementations Interoperability Test Suite

## Background

The [DIF Presentation Exchange Specification](https://identity.foundation/presentation-exchange/) codifies data format
to articulate proof requirements and proofs submitted. The implementors can choose to implement the standard in their
desired way. This node test suite project triggers several tests to analyze implementation's compliance with the
standard.

### Summary of Interactions:

* Validate that the presentation definition / request created by Verifier is as per DIF Specification
* Validate that the presentation submission created by Holder is as per DIF Specification
* Input evaluations: Verification, that presentation submissions fits presentation definition

## For Developers

This project has been created using:

* `yarn` version 1.22.10

### Install

```shell
yarn install
```

### Build

```shell
yarn build
```

### Test

The test command runs:

* `eslint`
* `prettier`
* `unit`

You can also run only a single section of these tests, using for example `yarn test:unit`.

```shell
yarn test
```

### Utility Scripts

There are several other utility scripts that help with development.

* `yarn fix` - runs `eslint --fix` as well as `prettier` to fix code style

## See Also

[New Implementor's Guide](./new_implementor.md)