import {Status} from '../models';
import {ResponseUtil} from '../utils';

export class SphereonResponseUtil implements ResponseUtil {
  private static instance: SphereonResponseUtil;

  public static getInstance(): SphereonResponseUtil {
    if (!SphereonResponseUtil.instance) {
      SphereonResponseUtil.instance = new SphereonResponseUtil();
    }

    return SphereonResponseUtil.instance;
  }

  getStatus(response: any): Status {
    const httpStatus = response['status'];

    if (response?.data?.result?.errors?.length > 0) {
      return Status.Error;
    }

    if (httpStatus >= 200 && httpStatus <= 299) {
      return Status.Success;
    }

    return Status.Error;

  }
}
