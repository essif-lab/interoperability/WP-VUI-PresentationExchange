import { Status } from '../models';
import { ResponseUtil } from '../utils';

export class GatacaResponseUtil implements ResponseUtil {
  private static instance: GatacaResponseUtil;

  public static getInstance(): GatacaResponseUtil {
    if (!GatacaResponseUtil.instance) {
      GatacaResponseUtil.instance = new GatacaResponseUtil();
    }

    return GatacaResponseUtil.instance;
  }

  getStatus(response: unknown): Status {
    // TODO Implement the function to translate the Company Specific Object to Status.
    return response != null ? Status.Success : Status.Error;
  }
}
