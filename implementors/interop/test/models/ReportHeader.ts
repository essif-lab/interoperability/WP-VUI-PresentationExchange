export interface ReportHeader {
  id: string;
  title: string;
}
