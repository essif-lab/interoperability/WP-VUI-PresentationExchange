import { IOCase } from './IOCase';

export class Config {
  implementorName: string;
  serviceAddress: string;
  tests: Array<IOCase>;
}
