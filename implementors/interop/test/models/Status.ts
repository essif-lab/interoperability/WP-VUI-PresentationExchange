export type Status = 'SUCCESS' | 'WARN' | 'ERROR' | 'INVALID';

export const Status = {
  Success: 'SUCCESS' as Status,
  Warn: 'WARN' as Status,
  Error: 'ERROR' as Status,
  Invalid: 'INVALID' as Status,
};
