import { HttpMethod } from './HttpMethod';
import { Status } from './Status';

/**
 * Interoperability test case properties.
 */
export interface IOCase {
  active: boolean;
  tag: string;
  url: string;
  httpMethod: HttpMethod;
  data: string;
  expectedStatus: Status;
}
