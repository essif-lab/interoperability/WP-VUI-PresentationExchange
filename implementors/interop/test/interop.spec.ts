import { DataProviders } from '../data';

import { IOCase } from './models';
import { ConfigsUtil, HttpUtil, ReportUtil, ResponseUtils } from './utils';

const configsUtil: ConfigsUtil = ConfigsUtil.getInstance();

describe('test rest api interoperability', () => {
  afterAll(() => {
    ReportUtil.getInstance().print();
  });

  test.each(configsUtil.ioCases)('.ioCase( %s )', async (ioCaseConfigKey: string) => {
    const responses = new Map<string, boolean>();

    const ioCaseConfig = configsUtil.ioRESTCaseConfigs.get(ioCaseConfigKey);

    if (ioCaseConfig) {
      for (const ioCaseConf of ioCaseConfig) {
        const ioCase: IOCase = ioCaseConf.tests[0];

        let data = null;
        let qs = '';

        const dataProvider = DataProviders.getInstance().getDataProvider(ioCaseConf.implementorName, ioCaseConfigKey);

        if (ioCase.data != null) {
          data = dataProvider.getRequestData(ioCaseConf.implementorName);
        }
        qs = dataProvider.getQueryParamData(ioCaseConf.implementorName);

        const response = await HttpUtil.request(ioCase.httpMethod, ioCaseConf.serviceAddress + ioCase.url + qs, data);

        dataProvider.setResponseData(response?.data);

        const responseFromAPI = ResponseUtils.getInstance().status(ioCaseConf.implementorName, response);

        responses.set(ioCaseConf.implementorName, responseFromAPI === ioCase.expectedStatus);
      }

      const numberOfResponses = responses.size;

      const numberOfResponsesWithExpectedStatus = Array.from(responses.entries())
        .map((entry) => entry[1])
        .filter((status) => status)
        .map(() => 1)
        .reduce((accumulator: number, currentValue: number) => accumulator + currentValue);

      ReportUtil.getInstance().addResults(ioCaseConfigKey, responses);

      expect(numberOfResponsesWithExpectedStatus).toEqual(numberOfResponses);
    }
  });
});
