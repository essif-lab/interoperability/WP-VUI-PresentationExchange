import { GatacaResponseUtil, SphereonResponseUtil } from '../implementation';
import { Implementor, Status } from '../models';

import { NoOpResponseUtil } from './NoOpResponseUtil';
import { ResponseUtil } from './ResponseUtil';

export class ResponseUtils {
  private static instance: ResponseUtils;

  private responseUtils: Map<string, ResponseUtil>;

  private constructor() {
    this.responseUtils = new Map<string, ResponseUtil>();
    this.responseUtils.set(Implementor.sphereon.toString(), SphereonResponseUtil.getInstance());
    this.responseUtils.set(Implementor.gataca.toString(), GatacaResponseUtil.getInstance());
  }

  public static getInstance(): ResponseUtils {
    if (!ResponseUtils.instance) {
      ResponseUtils.instance = new ResponseUtils();
    }

    return ResponseUtils.instance;
  }

  public status(implementor: string, response: unknown): Status {
    return this.getResponseUtil(implementor).getStatus(response);
  }

  private getResponseUtil(implementor: string) {
    let responseUtil: ResponseUtil = this.responseUtils.get(implementor);
    if (!responseUtil) {
      responseUtil = NoOpResponseUtil.getInstance();
    }

    return responseUtil;
  }
}
