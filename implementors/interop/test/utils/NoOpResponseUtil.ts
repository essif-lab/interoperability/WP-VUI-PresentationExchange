import { Status } from '../models';

import { ResponseUtil } from './ResponseUtil';

export class NoOpResponseUtil implements ResponseUtil {
  private static instance: NoOpResponseUtil;

  public static getInstance(): NoOpResponseUtil {
    if (!NoOpResponseUtil.instance) {
      NoOpResponseUtil.instance = new NoOpResponseUtil();
    }

    return NoOpResponseUtil.instance;
  }

  getStatus(response: unknown): Status {
    return response == null ? Status.Invalid : Status.Invalid;
  }
}
