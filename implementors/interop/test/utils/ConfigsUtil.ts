import fs from 'fs';

import { Config, Implementor } from '../models';

export class ConfigsUtil {
  private static instance: ConfigsUtil;

  uniqueIoCases: Set<string> = new Set();
  public ioCases: Array<string> = new Array<string>();
  public ioRESTCaseConfigs: Map<string, Config[]> = new Map();
  public ioLocalCaseConfigs: Map<string, Config[]> = new Map();

  private constructor() {
    this.init();
  }

  public static getInstance(): ConfigsUtil {
    if (!ConfigsUtil.instance) {
      ConfigsUtil.instance = new ConfigsUtil();
    }

    return ConfigsUtil.instance;
  }

  public static getFile(path: string) {
    const fileContent = fs.readFileSync(path, 'utf-8');
    return JSON.parse(fileContent);
  }

  init() {
    const configs: Config[] = Object.keys(Implementor)
      .map((implementor: Implementor) => '../' + implementor + '/config.json')
      .map((configFilePath: string) => ConfigsUtil.getFile(configFilePath));

    configs.forEach((config: Config) => {
      config.tests.forEach((ioCase) => {
        const simpleConfig: Config = new Config();
        simpleConfig.implementorName = config.implementorName;
        simpleConfig.tests = [ioCase];
        const tag = ioCase.tag;

        this.uniqueIoCases.add(tag);

        if (ioCase.active) {
          if (ioCase.url != null) {
            // Must be a interoperability test case for REST API
            simpleConfig.serviceAddress = config.serviceAddress;
            this.upsertIORESTCaseConfig(tag, simpleConfig);
          } else {
            this.upsertIOLocalTestCase(tag, simpleConfig);
          }
        }
      });
    });

    this.ioCases = Array.from(this.uniqueIoCases.values());
  }

  upsertIORESTCaseConfig(tag: string, simpleConfig: Config) {
    if (this.ioRESTCaseConfigs.get(tag) == null) {
      this.ioRESTCaseConfigs.set(tag, [simpleConfig]);
    } else {
      const simpleConfigs: Config[] = this.ioRESTCaseConfigs.get(tag);
      simpleConfigs.push(simpleConfig);
      this.ioRESTCaseConfigs.set(tag, simpleConfigs);
    }
  }

  upsertIOLocalTestCase(tag: string, simpleConfig: Config) {
    if (this.ioLocalCaseConfigs.get(tag) == null) {
      this.ioLocalCaseConfigs.set(tag, [simpleConfig]);
    } else {
      const simpleConfigs: Config[] = this.ioLocalCaseConfigs.get(tag);
      simpleConfigs.push(simpleConfig);
      this.ioLocalCaseConfigs.set(tag, simpleConfigs);
    }
  }
}
