import axios, {AxiosResponse} from 'axios';

import {HttpMethod} from '../models';

export class HttpUtil {
  public static async request(method: HttpMethod, url: string, data?: unknown) {
    let receivedResponse;

    try {

      let operation: Promise<AxiosResponse<any>> = null;

      if (method == HttpMethod.Get) {
        operation = axios.get(url);
      } else if (method == HttpMethod.Post) {
        operation = axios.post(url, data);
      }

      if (operation) {
        await operation.then((response) => {
          receivedResponse = response
        })
        .catch(({response}) => {
          receivedResponse = response
          // console.log(response.data);
          // console.log(response.status);
          // console.log(response.headers);
        });
      }
    } catch (exception) {
      receivedResponse = `ERROR received from ${url}: ${exception}\n`;
      process.stderr.write(receivedResponse);
    }

    return receivedResponse;
  }

}
