import {CsvWriter} from 'csv-writer/src/lib/csv-writer';

import {Implementor, ReportHeader} from '../models';

export class ReportUtil {
  private static REPORT_FILE_PATH = 'build/interop_report.csv';

  private static instance: ReportUtil;

  report: Map<string, Map<string, boolean>> = new Map();

  public static getInstance(): ReportUtil {
    if (!ReportUtil.instance) {
      ReportUtil.instance = new ReportUtil();
    }
    return ReportUtil.instance;
  }

  public addResults(ioCaseConfigKey: string, responses: Map<string, boolean>) {
    this.report.set(ioCaseConfigKey, responses);
  }

  public print() {
    const headerData: Array<ReportHeader> = [];

    headerData.push({ id: 'ioCase', title: 'Interoperability test case' });

    Object.keys(Implementor).forEach((implementor: string) => {
      headerData.push({ id: implementor.toLowerCase(), title: implementor });
    });

    const csvWriter = ReportUtil.initFile(headerData);

    const reportData = [];
    this.report.forEach((results: Map<string, boolean>, key: string) => {
      // console.log(key, results);
      const row = [];
      row['ioCase'] = key;

      results.forEach((result: boolean, implementor: string) => {
        row[implementor] = result;
      });

      reportData.push(row);
    });

    this.writeData(csvWriter, reportData);
  }

  private writeData(csvWriter: CsvWriter<any>, reportData: any[]) {
    csvWriter.writeRecords(reportData).then(() => {
      console.log('Interoperability results have been saved in the file.');
    });
  }

  private static initFile(headerData: Array<ReportHeader>): CsvWriter<any> {
    return require('csv-writer').createObjectCsvWriter({
      path: ReportUtil.REPORT_FILE_PATH,
      header: headerData,
    });
  }
}
