import { Status } from '../models';

export interface ResponseUtil {
  getStatus(response: unknown): Status;
}
