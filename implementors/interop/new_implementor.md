# New Implementor's Guide

This is a guide for the implementors of the presentation exchange for how they can test their implementation using this
interoperability test suite for compatibility with the other solutions.

The test suite has following structure:

The following contains the configuration that will list/define the test cases that an implementor wants to execute
against and match it's results against others.

The `tag` property is the `id` of the test case. All test cases with same ID will be passed the same data. The response
will be matched with the configured response.

    <RepoRoot>/implementors/implementorX/config.json

Following are two examples: for the REST API and the local lib integration testing.

```json
{
  "implementorName": "ImplementorX",
  "serviceAddress": "https://implementorx.com",
  "tests": [
    {
      "active": true,
      "tag": "create_presentation_definition",
      "url": "/{param}/definition",
      "httpMethod": "post",
      "data": "create_presentation_definition",
      "expectedStatus": "SUCCESS"
    },
    {
      "active": true,
      "tag": "validate_presentation_definition_structure",
      "data": "create_presentation_definition",
      "expectedStatus": "SUCCESS"
    }
  ]
}
```

Following file will contain the results of the interoperability tests:

    <RepoRoot>/implementors/implementorX/status.md

The following contains the input request body data that will be passed to implementation as data, this probably will NOT
require a change:

    <RepoRoot>/implementors/interop/data

Following contains how to translate the company specific response object to interoperability test suit's status.

    <RepoRoot>/implementors/interop/test/implementation/implementorXResponseUtil.ts

In following class the Response util will need to be registered:

    <RepoRoot>/implementors/interop/test/utils/ResponseUtils.ts

The following file will need to be updated with the name of the new implementor

    <RepoRoot>/implementors/interop/test/models/Implementor.ts

By following the examples mentioned in the files above a new implementor can add their implementation to the test suite
to test it and remain compatible with other implementations.


