# Status of Sphereon WP-NAME features

## Case 1 : Create Presentation Definition

- [ ] Create a presentation definition

## Case 2 : Retrieve Presentation Definition

- [ ] Retrieve a presentation definition

## Case 3 : Submit Verifiable Presentation

- [ ] Submit a verifiable presentation for a definition containing only one input descriptor with no constraints.
- [ ] Submit a verifiable presentation for a definition containing only one input descriptor with constraints.
- [ ] Submit a verifiable presentation for a definition containing more than one input descriptor with no constraints.
- [ ] Submit a verifiable presentation for a definition containing more than one input descriptor with constraints.
- [ ] Submit a verifiable presentation for a definition containing submission requirements

## Case 4 : Check Presentation Status

- [ ] Check presentation status
