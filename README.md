# Working Package Summary

Working Package: 1

Working Package Name: Presentation Exchange API

Status: Open Call

Start date: 15-Apr-2021

## Participants

> A Working Package must have at least two implementors and one integrator

| Role | Company | Contact Name | Contact Email
|---|---|---|---|
| Coordinator | GATACA | Irene Hernandez| irene@gataca.io
| Implementor| GATACA| Jose San Juan| jose@gataca.io
| Implementor | Sphereon |  Joris Nijman |  jnijman@sphereon.com |  
| Implementor | iGrant.io | Lal Chandran | lal@igrant.io | 
| Integrator |  Letstrust | Dominik Beron | dominik@letstrust.id |
| Integrator |  WordpreSSI |  Massimo Romano |  massimo.romano@associazioneblockchain.it|

# Terminology

* **Decentralized Identifier (DID)** as defined in [DID-CORE](https://gataca-io.github.io/verifier-apis/#bib-did-core)
  
* **DID document** as defined in [DID-CORE](https://gataca-io.github.io/verifier-apis/#bib-did-core)

* **DID resolution** as defined in [DID-RESOLUTION](https://gataca-io.github.io/verifier-apis/#bib-did-resolution)
  
* **Revocation List 2020** as defined in [REVOCATION-LIST-2020](https://w3c-ccg.github.io/vc-status-rl-2020/)   
  
* **Verifier** as defined in [VC-DATA-MODEL](https://gataca-io.github.io/verifier-apis/#bib-vc-data-model)

* **Verifiable Credential (VC)** as defined in [VC-DATA-MODEL](https://gataca-io.github.io/verifier-apis/#bib-vc-data-model)

* **Verifiable Presentation (VP)** as defined in [VC-DATA-MODEL](https://gataca-io.github.io/verifier-apis/#bib-vc-data-model)


## Scope

The Presentation Exchange API shall follow the objectives set by the VUI. The motivation is to build scalable
interoperability solutions between wallet- and verifier- technology providers. That is, to make any wallet adhering to the protocol be able to successfully exchange credentials with
any verifier adhering to the protocol.

As per a survey conducted by this group 100% of survey respondents provided mobile wallets, while only 63% provide
cloud-based wallets and 54% provide desktop-based wallets. Therefore, protocol standarization efforts should cover at least (but not only) mobile wallets. Compared to other
standards this effort should provide a guide for the development of wallets that want to share credentials with any
verifier.

Following this reasoning, the architecture design constraints are the following:
- There must be a specific SSIExchange Layer just for the Verifiable Presentation Exchange
- Wallets need to share credentials directly with Verifiers applications, not accross other providers applications.
- The SSIExchange Layer must be usable for any business case (authentication, signing, data-sharing...) or application protocol (OIDC, SAML..) above it.
- The SSIExchange Layer may use underneath an SSI Verification Layer to request cryptographic validation of presentations or credentials, such as the ones suggesteds on [VC-HTTP-APIS](https://w3c-ccg.github.io/vc-http-api/) or the EssifLab- EidasBridge Interop effort.

That would imply (approximately) the following features
* [Minimal data requirement / Selective disclosure]()
* [Consent management of credential sharing]()
* [Source Wallet identification]()
* [Mobile wallet support - outbound authentication -]()
* [Application layer agnosticity]()
  
## Standard drafts and interoperability specs to be consulted 

  [TO BE UPDATED]

Scope item | References
--- | --- 
Presentation request data model | [DIF Presentation Exchange](https://identity.foundation/presentation-exchange/); [Aries RFC 0037: Present Proof Protocol 1.0](https://github.com/hyperledger/aries-rfcs/blob/master/features/0037-present-proof/README.md)
Presentation response data model | [DIF Presentation Exchange](https://identity.foundation/presentation-exchange/); [Aries RFC 0037: Present Proof Protocol 1.0](https://github.com/hyperledger/aries-rfcs/blob/master/features/0037-present-proof/README.md)
QR data model | 
Deeplink data model | 
Consent data model | [DIF Credential Manifest](https://identity.foundation/credential-manifest/); [Aries RFC 0167: Data Consent Lifecycle](https://github.com/hyperledger/aries-rfcs/blob/master/concepts/0167-data-consent-lifecycle/README.md)
Presentation exchange protocol |  [DIF Presentation Exchange](https://identity.foundation/presentation-exchange/); [Aries RFC 0037: Present Proof Protocol 1.0](https://github.com/hyperledger/aries-rfcs/blob/master/features/0037-present-proof/README.md); [SIOP DID Profile v0.1](https://identity.foundation/did-siop/#:~:text=This%20specification%20defines%20the%20%22SIOP,Identity%20Wallets%20into%20their%20web)
Consent exchange protocol | [DIF Credential Manifest](https://identity.foundation/credential-manifest/)| 

## Participate:
* **Apply as Implementor to this WP**: Create a Merge Request on GitLab editing the ReadMe of the corresponding WP. Set the existing WP Coordinator as reviewer.

* **Apply as Integrator to this WP**: Create a Merge Request on GitLab editing the ReadMe of the corresponding WP. Set the existing WP Coordinator as reviewer.

* **File a bug or comments to the WP**:
  - Create new Issues
  - Comment on existing Issues
  - Suggest improvement by opening a Merge Requests of the repository (publicly available for reading, need a Gitlab account for writing)
  - Or else [Mail To: vui@groups.io](mailto://vui@groups.io)

* **Request Access to GitLab**, or any other issue or process if no existing access to GitLab: [Mail To: vui@groups.io](mailto://vui@groups.io)
