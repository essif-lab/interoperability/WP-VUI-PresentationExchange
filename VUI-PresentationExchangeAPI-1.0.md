# VUI - Presentation Exchange v1.0

* Authors: 
* Status: draft
* Start date: 
* Last update: 

## Table of contents
* [Summary](#summary)
* [Assumptions](#assumptions)
* [Normative references](#normative-references)
* [Definitions](#definitions)
* [Data Model](#data-model)
    * [Presentation request data model]()
    * [Presentation response data model]()
    * [Consent data model]()
* [Features](#features)
    * [Minimal data requirement / Selective disclosure]()
    * [Consent management of credential sharing]()
    * [Source Wallet identification]()
    * [Mobile wallet support - outbound authentication -]()
    * [Application layer agnosticity]()
* [Protocols](#protocols)
    * [QR data model]()
    * [Deeplink data model]()
    * [Presentation exchange API]()
    * [Issuer Resolution API]()
    * [Presentation request protocol]()
    * [Presentation response protocol]()
    * [Consent exchange protocol]()
* [Implementation Guidelines](#implementation-guidelines)